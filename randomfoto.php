<?php
$baseDir = "img/";

if(isset($_GET["groep"])){
	//de pagina wordt aangeroepen  vanuit javascript. De page (ongetwijfeld de groep vanwaar een foto moet gehaald worden) moet ervoor komen te staan. ook een directory wijziging is nodig
	$page = $_GET["groep"];
	echo $page . "|";
	$page = "groepen/".$page;
}

$d = $baseDir . $page . "/";
$dir = dir($d);


function hasImageExtension($file){
	$extensions = array("png", "jpg", "gif", "jpeg");
	$temp = explode(".",$file);
	$ext = strtolower(end($temp));
	return in_array($ext,$extensions);
}

//maak lijst met alle foto's in de directory
$list = array();
//hier directory lees code
while(false !== ($entry = $dir->read())) {
	$file = $d . $entry;
	if(is_file($file) && hasImageExtension($file)) {
		$list[] = $file;
	}
}

$index = rand(0,count($list)-1);
$img = $list[$index];
if(is_file($img)){
	echo '<a href="'.$img.'"><img src="'.$img.'" class="randomFoto"/></a>';
}
?>