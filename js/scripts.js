var info = new Array();
var groepen = new Array();

groepen["Jette"] = new Array("jette");
groepen["Sint - Pieters - Woluwe"] = new Array("mooibos", "rnb");
groepen["Sint - Lambrechts - Woluwe"] = new Array("kruisboog");
groepen["Sint - Jans - Molenbeek"] = new Array("scogimo");
groepen["Evere"] = new Array("evere");
groepen["Laken & Neder - Over - Heembeek"] = new Array("ruusbroec");
groepen["Ukkel"] = new Array("ukkel");
groepen["Ganshoren"] = new Array("ganshoren","akabe");

info["jette"] = '<p><font size="2" face="Verdana"><b><u>Scouts en Gidsen De Klauwaert - Jette </u></b></font><br /><font size="2" face="Verdana">Wemmelsesteenweg 310</font><br /><font size="2" face="Verdana">1090 Jette</font><br /><a href="http:\/\/www.scoutsjette.be/" target="_blank"><font color="#0000FF" size="2" face="Verdana">http:\/\/www.scoutsjette.be</font></a><div id="jette"></div></p>';

info["akabe"] = '<p><font size="2" face="Verdana"><b><u>Akabe Brussel </u></b></font><br /><font size="2" face="Verdana">L. Demesmaekerstraat 53</font><br /><font size="2" face="Verdana">1083 Ganshoren </font><br /><font size="2" face="Verdana"><a href="http:\/\/www.akabebrussel.be" target="_blank">http:\/\/www.akabebrussel.be</a></font><div id="akabe"></div></p>';

info["mooibos"] = '<p><font size="2" face="Verdana"><b><u>Scouts en Gidsen Mooibos-Lievens </u></b></font><br /><font size="2" face="Verdana">Vander Meerschenlaan 165</font><br /><font size="2" face="Verdana">1150 Sint-Pieters-Woluwe</font><br /><a href="http:\/\/www.mooibos-lievens.be/" target="_blank"><font size="2" face="Verdana">http:\/\/www.mooibos-lievens.be</font></a><font size="2" face="Verdana"> </font> <br><div id="mooibos"></div></p>';

info["rnb"] = '<p><font size="2" face="Verdana"><b><u>Scouts en Gidsen Reynaert-Beatrijs </u></b></font><br /><font size="2" face="Verdana">Vogelzanglaan 2</font><br /><font size="2" face="Verdana">1150 Sint-Pieters-Woluwe</font><br /><font size="2" face="Verdana"><a href="http:\/\/reynaert-beatrijs.scoutnet.be" target="_blank">http:\/\/reynaert-beatrijs<WBR>.scoutnet.be</a></font> <br><div id="rnb"></div></p>';

info["kruisboog"] = '<p><font size="2" face="Verdana"><b><u>Scouts en Gidsen Kruisboog- Hadewych</u></b></font><br /><font size="2" face="Verdana">Gebouw &quot;302&quot;</font><br /><font size="2" face="Verdana">Roodebeeksteenweg 302</font><br /><font size="2" face="Verdana">1200 Sint-Lambrechts-Woluwe.</font><br /><font size="2" face="Verdana"><a href="http:\/\/www.kruisboog-hadewijch.be" target="_blank">http:\/\/www.kruisboog-hadewijch<WBR>.be</a></font> <br><div id="kruisboog"></div></p>';

info["scogimo"] = '<p><font size="2" face="Verdana"><b><u>Scouts en Gidsen Molenbeek </u></b></font><br /><font size="2" face="Verdana">Haeckstraat 59</font><br /><font size="2" face="Verdana">1080 Molenbeek</font> <br><a href="http://scogimo.districtbrussel.be">http://scogimo.districtbrussel.be</a><div id="scogimo"></div></p>';

info["evere"] = '<p><font size="2" face="Verdana"><b><u>Scouts en Gidsen Ridders van O.-L.-V. Onbevlekt - Evere </u></b></font><br /><font size="2" face="Verdana">Hendrik Consciencelaan 156</font><br /><font size="2" face="Verdana">1140 Evere</font><br /><a href="http:\/\/www.scoutsevere.be/" target="_blank"><font size="2" face="Verdana">http:\/\/www.scoutsevere.be</font></a><font size="2" face="Verdana"> </font> <br><div id="evere"></div></p>';

info["ruusbroec"] = '<p><font size="2" face="Verdana"><b><u>Scouts en Gidsen Ruusbroec - Laken </u></b></font><br /><font size="2" face="Verdana">Forumlaan 4</font><br /><font size="2" face="Verdana">1020 Laken </font><br /><font size="2" face="Verdana"><a href="http:\/\/www.scoutsruusbroec.be/" target="_blank">http:\/\/www.scoutsruusbroec.be/</a></font> <br><div id="ruusbroec"></div></p>';

info["ukkel"] = '<p><font size="2" face="Verdana"><b><u>Scouts en Gidsen Ekwator - Jan Breydel Ukkel </u></b></font><br /><font size="2" face="Verdana">Dekenijstraat 100</font><br /><font size="2" face="Verdana">1180 Ukkel </font><br /><font size="2" face="Verdana"><a href="http:\/\/www.scoutsukkel.be/" target="_blank">http:\/\/www.scoutsukkel.be/</a></font> <br><div id="ukkel"></div></p>';

info["ganshoren"] = '<p><font size="2" face="Verdana"><b><u>Scouts en Gidsen Sint-Martinus - Ganshoren </u></b></font><br /><font size="2" face="Verdana">L. Demesmaekerstraat 53</font><br /><font size="2" face="Verdana">1083 Ganshoren </font><br /><font size="2" face="Verdana"><a href="http:\/\/www.scoutsganshoren.be/" target="_blank">http:\/\/www.scoutsganshoren.be/</a></font> <br><div id="ganshoren"></div></p>';

var geenGroep = "In deze gemeente bevind zich geen groep. De groene gebieden zijn de gemeentes met scoutsgroepen.";

/**
 *
 * @access public
 * @return void
 **/
function getInfo(locatie){
	if(groepen[locatie] == undefined){
		document.getElementById('info').innerHTML = geenGroep;
	}else{
		gr = groepen[locatie];
		document.getElementById('info').innerHTML = "";
		for(i in gr){
			addGroepInfo(gr[i]);
		}
		//showImage(locatie);
	}
}

function addGroepInfo(groep){
	code = document.getElementById('info').innerHTML;
	code += info[groep];
	document.getElementById('info').innerHTML = code;
	showImg(groep);
}

function showImg(groep){
	//hier wordt met een xmlhttprequest randomFoto.php aangeroepen om zo een randomfoto van deze groep op te vragen en in de div te plaatsen.
	var lHttp = createRequestObject();
	lHttp.open('get', 'randomfoto.php?groep='+groep);
    lHttp.onreadystatechange = handleResponse;
    
    ajaxEnqueue(lHttp);
    if(!busy){
    	executeAjax();
    }
    
    //http.send(null);
}

function createRequestObject() {
    var ro;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
        ro = new ActiveXObject("Microsoft.XMLHTTP");
    }else{
        ro = new XMLHttpRequest();
    }
    return ro;
}

var http = null;
var busy = false;

function handleResponse() {
    if(http.readyState == 4){
        var response = http.responseText;
        var update = new Array();

        if(response.indexOf('|' != -1)) {
            update = response.split('|');
            document.getElementById(update[0]).innerHTML = update[1];
        }
    	busy = false;
    	executeAjax();
    }
}

function showToolTip(locatie){
	var element = document.getElementById("tooltip");
	element.innerHTML = locatie;
}

function executeAjax(){
	if(ajaxQueue.length > 0){
		busy = true;
		http = ajaxDequeue();
		http.send(null);
	}
}
var ajaxQueue = new Array()

/*
 *Add element to the queue and execute the request if needed
 */
function ajaxEnqueue(request){
	ajaxQueue.push(request);
}

/**
 * Get and remove element from the queue
 */
function ajaxDequeue(){
	return ajaxQueue.shift();
	
}