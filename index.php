<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="og:title" content="Scouts en Gidsen Brussel" />
<meta property="og:description" content="Scouts en Gidsen Brussel bestaat uit tien groepen verspreid in het Brussels Hoofdstedelijk Gewest. Kom je meedoen? Op onze site vind je zeker een groep in je buurt. Ook  kinderen en jongeren met een beperking kunnen in Brussel terecht en bij uitstek bij Akabe Brussel in Ganshoren!" />
<meta property="og:image" content="http://www.scoutsengidsenbrussel.be/img/logo-district-brussel.jpg" / >

<title>Scouts en Gidsen Brussel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/scripts.js"></script>
<script type=”text/JavaScript”>
  function showEmail(){
    var voor = "regiobrussel";
    var at = "@";
    var achter = "scoutsengidsenvlaanderen.be";
    var address = <a href="mailto:' + voor + at + achter'">regiobrussel [at] scoutsengidsenvlaanderen.be</a>');
    if (document.getElementById("email") == undefined) {
      document.getElementById("email").innerHTML = address;
    }
    
  }
  </script>
</head>
<body>
<div class="main">
<div class="banner">

</div>
<div class="topMenu">
<ul>
<li class="menu"><a href="index.php?page=main" class="menu">Scouting</a></li>
<li class="menu"><a href="index.php?page=groepen" class="menu">Groepen</a></li>
<li class="menu"><a href="index.php?page=contact" class="menu">Contact</a></li>
</ul>
</div>
<div class="content">
<?php
if(!isset($_GET["page"])){
	$page = "main";
} else {
	$page = $_GET["page"];
}
$pageWithExtension = $page . ".php";
if(file_exists($pageWithExtension)){
	include ($pageWithExtension);
} else {
	include("main.php");
}


?>

</div>
<div class="bottom">
<br />
<a href="http://www.scoutsengidsenvlaanderen.be">Scouts en Gidsen Vlaanderen Nationaal</a> - 
<a href="http://www.gouwwebra.be">Gouw West-Brabant</a>
</div>
</div>
</body>
</html>