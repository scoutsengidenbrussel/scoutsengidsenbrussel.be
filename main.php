<div class="container">
	<img src="img/takwerking.png" class="content_image" usemap="#groepen"/>
    <map name="groepen">
        <area shape="rect" coords="200,10,500,90" href="http://www.scoutsengidsenvlaanderen.be/kijk-op-scouting/takwerking/kapoenen" alt="Kapoenen" title="Kapoenen"/>
        <area shape="rect" coords="50,90,550,190" href="http://www.scoutsengidsenvlaanderen.be/kijk-op-scouting/takwerking/kabouters-welpen" alt="Kabouters en Welpen" title="Kabouters en Welpen"/>
        <area shape="rect" coords="50,190,580,345" href="http://www.scoutsengidsenvlaanderen.be/kijk-op-scouting/takwerking/jonggidsen-jongverkenners" alt="Jonggidsen en Jongverkenners" title="Jonggidsen en Jongverkenners"/>
        <area shape="rect" coords="120,345,550,435" href="http://www.scoutsengidsenvlaanderen.be/kijk-op-scouting/takwerking/gidsen-verkenners" alt="Gidsen en Verkenners" title="Gidsen en Verkenners"/>
        <area shape="rect" coords="70,420,450,540" href="http://www.scoutsengidsenvlaanderen.be/kijk-op-scouting/takwerking/jins" alt="Jins" title="Jins"/>
        <area shape="rect" coords="80,540,450,630" href="http://www.scoutsengidsenvlaanderen.be/kijk-op-scouting/takwerking/groepsleiding" alt="Groepsleiding" title="Groepsleiding"/>
    </map>

	<span class="content_tekst">
<h3> Scouts en Gidsen Brussel </h3>
<br />
<p>
		Wij zijn scouts en gidsen, meisjes en jongens,
</p><p>
elk met een eigen verhaal. Iedereen kan erbij.
</p><p>
We gaan samen op verkenning en durven tuimelen in het leven.
</p><p>
De natuur is onze troef maar ook Brussel zit in onze binnenzak
</p><p>
We geloven in onszelf, in elkaar en in iets meer.
</p><p>
We spelen een spel dat niet luchtledig is,
</p><p>
in vrije tijd die niet vrijblijvend is.
</p><p>
Met groot plezier en kleine daden
</p><p>
komen we op voor onze omgeving en
</p><p>
voor een bruisend Brussel.
</p><p>
Zo dromen we luidop van gelukkige mensen
</p><p>
in een rechtvaardige wereld.
</p>

<img src="img/vgc.jpg" alt="Met de steun van de VGC" title="Met de steun van de VGC" />

<?php require_once("randomfoto.php"); ?>
	</span>
</div>