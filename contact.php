<div class="content_image"><p>
<?php require_once("randomfoto.php"); ?>
</div>
<span class="content_tekst">

<p>Scouts en Gidsen Brussel tellen bijna 1000 leden en leiding verspreid over 10 groepen in het Brussels Hoofdstedelijk Gewest!!  Er is dus vast een groep in je buurt.  </p>

<p>Ook voor kinderen en jongeren met een beperking is er een groep in Brussel, je vindt hen terug in Ganshoren! (zie "<a href="index.php?page=groepen">Groepen</a>") </p>


<p>Op zoek naar de contactgegevens van één van de Brusselse scouts- en gidsengroepen? Op de contactgegevens <a href="index.php?page=groepen">hiernaast</a> kan je altijd terecht. </p>

<p>Heb je nog een vraag over scouting in Brussel, of wil je meer informatie over onze groepen of scouting in Brussel, aarzel dan zeker niet om Trees Vandenbulcke te contacteren!  </p>
<p>
Naomi Costrop
<br />
Stadsondersteunster Scouts en Gidsen Brussel
<br />
Kolenmarkt 85
<br />
1000 Brussel
<br />
02/411.25.35 | 0498/13.48.01.
<br />
regiobrussel [at] scoutsengidsenvlaanderen.be
<div id="email"></div>
<br /><br />
<a href="index.php?page=groepen"><img src="img/meedoen.gif" /></a>
<p>Informatie over vrijwilliger-zijn bij Scouts en Gidsen Vlaanderen vind je <a href="http://www.scoutsengidsenvlaanderen.be/dienstverlening/goed-geregeld/statuut-vd-vrijwilliger">hier</a>.</p>
</p>
</span>